const pjson = require("./package.json");

process.env.VUE_APP_VERSION = pjson.version;
process.env.VUE_APP_AUTHOR_NAME = pjson.author.name;
process.env.VUE_APP_AUTHOR_EMAIL = pjson.author.email;
process.env.VUE_APP_AUTHOR_URL = pjson.author.url;

module.exports = {
  pwa: {
    // configure the workbox plugin
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: "public/service-worker.js"
      // ...other Workbox options...
    }
  }
};
