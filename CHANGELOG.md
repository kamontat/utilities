<a name="unreleased"></a>
## [Unreleased]


<a name="v1.1.0"></a>
## [v1.1.0] - 2019-05-15
### Feature
- **ui:** add confirm dialog when click logout

### Fixes Bug
- **icon:** update new real ico image
- **icon:** add favicon due to code error

### Improving application
- **cache:** make all image caches


<a name="v1.0.1"></a>
## [v1.0.1] - 2019-05-12
### Fixes Bug
- **ui:** location title missing in the first load


<a name="v1.0.0"></a>
## v1.0.0 - 2019-05-12
### Feature
- **auth:** update auth workflow
- **chart:** implement chart to water and elec
- **dashboard:** make dashboard element be pluginable
- **electricity:** implement electricity input and final it
- **input:** upgrade inputbar for water
- **location:** integrate home and condo together
- **login:** integrate login with firebase auth
- **price:** implement price tag and units
- **pwa:** try to integrate PWA to website
- **router:** add router for the web
- **ui:** add interactive table to show record
- **ui:** add dots that represent record in calendar
- **ui:** install price tag and trend chart to dashboard
- **ui:** add calendar to dashboard

### Fixes Bug
- **calendar:** upgrade dependency to fixes dup month
- **chart:** incorrect time chart
- **chart:** make chart auto update base on input date
- **functions:** price should return 0 if no unit exist
- **path:** update path resolver
- **test:** change default testing
- **test:** make e2e be headless
- **ui:** break field in iphone

### Improving application
- download node version to 8
- **analytic:** add google analytic
- **asset:** remove messive logo and reduce request
- **auth:** add login flow
- **chart:** include chart to dashboard
- **deployment:** update deployment folder to dist
- **error:** improve message with error occurred
- **firebase:** add more firebase and setup
- **functions:** make functions support cors and avoid error
- **functions:** add new calculate firebase function
- **lib:** add buefy, change function region
- **log:** remove perf log
- **logo:** change vue logo to our brand
- **meta:** add seo meta data to website
- **monitor:** include firebase performance monitoring
- **number:** increase water decimal to 0.0001
- **style:** update color and font
- **table:** add paginate
- **ui:** add margin between dashboard tools and dashboard
- **ui:** create improvement ui
- **ui:** improve some calendar colors
- **utilities:** add firebase error

### Pull Requests
- Merge branch 'feature/calendar' into 'master'


[Unreleased]: https://gitlab.com/kamontat/utilities/compare/v1.1.0...HEAD
[v1.1.0]: https://gitlab.com/kamontat/utilities/compare/v1.0.1...v1.1.0
[v1.0.1]: https://gitlab.com/kamontat/utilities/compare/v1.0.0...v1.0.1
