#!/usr/bin/env bash
# shellcheck disable=SC1000

# generate by create-script-file v4.0.0
# link (https://github.com/Template-generator/create-script-file/tree/v4.0.0)

# set -x #DEBUG - Display commands and their arguments as they are executed.
# set -v #VERBOSE - Display shell input lines as they are read.
# set -n #EVALUATE - Check syntax of the script but don't execute.

#/ -----------------------------------
#/ Description:  One input (major|minor|patch)
#/ -----------------------------------
#/ Create by:    Kamontat Chantrachirathunrong <kamontat.c@hotmail.com>
#/ Since:        12/05/2019
#/ -----------------------------------
#/ Error code    1      -- error
#/ -----------------------------------
#// Version:      1     -- First release

if [[ $# -lt 1 ]]; then
  echo "Require one input! (major|minor|patch)"
  exit 1
fi

VERSION_FLAGS=("$@")

VERSION="$(npm version --no-git-tag-version "${VERSION_FLAGS[@]}")"

gitgo changelog --tag "$VERSION"                      # create changelog
git commit -am "chore(release): update to ${VERSION}" # commit all file
git tag "$VERSION"                                    # create git tag

git push
git push --tags
