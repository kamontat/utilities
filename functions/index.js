const functions = require("firebase-functions");
const calculate = require("./src/calculate");

const cors = require("cors")({
  origin: true
});

exports.calculate = functions
  .region("asia-northeast1")
  .https.onRequest((req, res) => {
    return cors(req, res, () => {
      calculate.default(req, res);
    });
  });
