const ELECTRICITY_PRICE_RULES = {
  ft: -0.116,
  high: {
    "150": 3.2484,
    "400": 4.2218,
    "2000": 4.4217,
    service: 38.22
  },
  low: {
    "15": 2.3488,
    "25": 2.9882,
    "35": 3.2405,
    "100": 3.6237,
    "150": 3.7171,
    "400": 4.2218,
    "2000": 4.4217,
    service: 8.19
  }
};

exports.default = number => {
  if (!number || typeof number !== "string") {
    return 0;
  }

  const unit = parseInt(number, 10); // unit number

  const rule =
    unit > 150 ? ELECTRICITY_PRICE_RULES.high : ELECTRICITY_PRICE_RULES.low;
  const service = rule.service;
  const condition = {
    ...rule
  };
  delete condition.service;

  let price = 0;
  let previous = 0;
  for (let key in condition) {
    const number = parseInt(key, 10);
    const rate = condition[key];

    if (unit <= number) {
      price += (unit - previous) * rate;
      break; // terminate condition
    } else {
      price += (number - previous) * rate;
      previous = number;
    }
  }

  if (price === 0) return 0;

  const base = price + service;
  const ft = unit * ELECTRICITY_PRICE_RULES.ft;
  const final = base + ft;
  const vat = (final * 7) / 100; // 7%
  return (final + vat).toFixed(2);
};
