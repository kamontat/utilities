const WATER_PRICE_PER_UNIT = 18;

exports.default = number => {
  if (!number || typeof number !== "string") {
    return 0;
  }

  return parseInt(number, 10) * WATER_PRICE_PER_UNIT;
};
