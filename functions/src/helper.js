exports.buildQuery = query => {
  return {
    query
  };
};

exports.buildStatus = (status, message) => {
  return {
    status: {
      code: status,
      detail: message
    }
  };
};

exports.buildResult = message => {
  return {
    result: message
  };
};
