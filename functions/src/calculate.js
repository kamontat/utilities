const helper = require("./helper");

const water = require("./calculate/water").default;
const electricity = require("./calculate/electricity").default;

exports.default = (req, res) => {
  let result = undefined;

  try {
    if (req.query.type === "w") {
      result = water(req.query.unit);
    } else if (req.query.type === "e") {
      result = electricity(req.query.unit);
    } else {
      throw Error("type not found");
    }
  } catch (e) {
    return res.status(400).json({
      ...helper.buildQuery(req.query),
      ...helper.buildStatus(400, e.message)
    });
  }

  return res.status(200).json({
    ...helper.buildResult(result),
    ...helper.buildQuery(req.query),
    ...helper.buildStatus(200, "successful")
  });
};
