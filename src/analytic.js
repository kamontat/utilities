import Vue from "vue";
import VueAnalytics from "vue-analytics";

import router from "./router";

// https://www.npmjs.com/package/vue-analytics
Vue.use(VueAnalytics, {
  id: "UA-139654704-2",
  router,
  autoTracking: {
    screenview: true
  }
});
