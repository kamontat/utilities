import Vue from "vue";
import Vuex from "vuex";

import security from "./stores/security";

import water from "./stores/water";
import electricity from "./stores/electricity";

import location from "./stores/location";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    security,
    water,
    electricity,
    location
  }
});

export default store;
