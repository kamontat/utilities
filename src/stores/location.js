import logger from "@/logger.js";

export default {
  namespaced: true,
  state: {
    name: "condo"
  },
  mutations: {
    set: (state, location) => {
      if (!location) return;

      logger.location(`Set location state to ${location}`);

      localStorage.setItem("location", location); // update local storage
      state.name = location;
    }
  }
};
