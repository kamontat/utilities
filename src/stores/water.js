import logger from "@/logger.js";

export default {
  namespaced: true,
  state: {
    list: {
      condo: [],
      home: []
    }
  },
  getters: {
    nextID: state => {
      return {
        home: state.list.home.length,
        condo: state.list.condo.length
      };
    },
    latest: state => {
      let lc = undefined;
      let lh = undefined;

      if (state.list.condo.length > 0)
        lc = state.list.condo[state.list.condo.length - 1];
      if (state.list.home.length > 0)
        lh = state.list.home[state.list.home.length - 1];

      return {
        condo: lc,
        home: lh
      };
    }
  },
  mutations: {
    add(state, { water, location }) {
      if (
        typeof water.timestamp === "string" ||
        typeof water.timestamp === "number"
      )
        water.timestamp = new Date(water.timestamp);
      logger.utility(`add new water element (%O)`, water);
      state.list[location].push(water);
    }
  },
  actions: {
    add(context, object) {
      context.commit("add", object);
    }
  }
};
