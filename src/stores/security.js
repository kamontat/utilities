import firebase from "@/firebase.js";
import "firebase/auth";

import logger from "@/logger.js";

export default {
  namespaced: true,
  state: {
    authentication: undefined,
    isAuthenticated: false
  },
  getters: {
    phone: state => {
      if (state.authentication) {
        return state.authentication.phoneNumber;
      }
      return undefined;
    }
  },
  mutations: {
    login(state, user) {
      logger.auth(`start login with ${user.phoneNumber}`);

      state.isAuthenticated = true;
      state.authentication = user;
    },
    logout(state) {
      logger.auth("start logout");
      firebase.auth().signOut();

      state.isAuthenticated = false;
      state.authentication = undefined;
    }
  },
  actions: {
    login(context, user) {
      context.commit("login", user);
    },
    logout(context) {
      context.commit("logout");
    }
  }
};
