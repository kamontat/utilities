import logger from "@/logger.js";

export default {
  namespaced: true,
  state: {
    list: {
      condo: [],
      home: []
    }
  },
  getters: {
    nextID: state => {
      return {
        home: state.list.home.length,
        condo: state.list.condo.length
      };
    },
    latest: state => {
      let lc = undefined;
      let lh = undefined;

      if (state.list.condo.length > 0)
        lc = state.list.condo[state.list.condo.length - 1];
      if (state.list.home.length > 0)
        lh = state.list.home[state.list.home.length - 1];

      return {
        condo: lc,
        home: lh
      };
    }
  },
  mutations: {
    add(state, { electricity, location }) {
      if (
        typeof electricity.timestamp === "string" ||
        typeof electricity.timestamp === "number"
      )
        electricity.timestamp = new Date(electricity.timestamp);
      logger.utility(
        "add new electricity element (%O) to %s",
        electricity,
        location
      );

      state.list[location].push(electricity);
    }
  },
  actions: {
    add(context, object) {
      context.commit("add", object);
    }
  }
};
