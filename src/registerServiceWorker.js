/* eslint-disable no-console */

import { register } from "register-service-worker";
import logger from "./logger.js";

if (process.env.NODE_ENV === "production") {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready() {
      logger.sw(
        "App is being served from cache by a service worker.\n" +
          "For more details, visit https://goo.gl/AFskqB"
      );
    },
    registered() {
      logger.sw("Service worker has been registered.");
    },
    cached() {
      logger.sw("Content has been cached for offline use.");
    },
    updatefound() {
      logger.sw("New content is downloading.");
    },
    updated(registration) {
      logger.sw("New content is available; please refresh.");

      let worker = registration.waiting;
      worker.postMessage({
        action: "skipWaiting"
      });

      logger.sw("Finish refresh content.");
    },
    offline() {
      logger.sw(
        "No internet connection found. App is running in offline mode."
      );
    },
    error(error) {
      console.error("Error during service worker registration:", error);
    }
  });
}
