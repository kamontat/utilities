import firebase from "firebase/app";
// Add the Performance Monitoring library
import "firebase/performance";

const firebaseConfig = {
  apiKey: "AIzaSyDVYMNT5tGpcL0VlAOjl1tk6w5-6ervxb8",
  authDomain: "condo-utilities.firebaseapp.com",
  databaseURL: "https://condo-utilities.firebaseio.com",
  projectId: "condo-utilities",
  storageBucket: "condo-utilities.appspot.com",
  messagingSenderId: "717689992733",
  appId: "1:717689992733:web:1010ad93791f9082"
};

firebase.initializeApp(firebaseConfig);
// Initialize Performance Monitoring and get a reference to the service
firebase.performance();

export default firebase;
