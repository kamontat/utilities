import Vue from "vue";
import Router from "vue-router";

import logger from "./logger.js";
import store from "./store";

import Dashboard from "./views/Dashboard.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: Dashboard
    },
    {
      path: "/security",
      name: "security",
      component: () => import("./views/Security.vue")
    },
    {
      path: "/water",
      name: "water",
      component: () => import("./views/Water.vue")
    },
    {
      path: "/electricity",
      name: "electricity",
      component: () => import("./views/Electricity.vue")
    },
    {
      path: "/logout",
      name: "logout",
      component: () => import("./views/Logout.vue")
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (from.name === to.name) return next();

  if (to.name === "logout") {
    logger.router(`redirect logout`);
    store.dispatch("security/logout");
    return next({
      name: "dashboard",
      query: from.query
    });
  }

  logger.router(`from: ${from.name} => to: ${to.name}`);

  if (to.name === "security") {
    logger.router(`redirect to security`);
    if (store.state.security.isAuthenticated) {
      logger.router(`already authenticated`);
      return next({
        name: to.query.redirect
      });
    }
  }

  if (from.name === "security") {
    logger.router(`redirect from security`);
    if (!store.state.security.isAuthenticated) {
      logger.router(`didn't authenticate yet`);
      return next({
        replace: true,
        name: `security`,
        query: {
          redirect: to.name
        }
      });
    }
  } else if (/(water|electricity)/.test(to.name)) {
    logger.router(`one of security path`);
    if (!store.state.security.isAuthenticated) {
      logger.router(`never authenticated`);
      return next({
        replace: true,
        name: `security`,
        query: {
          redirect: to.name
        }
      });
    }
  }

  return next();
});

export default router;
