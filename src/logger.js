const debug = require("debug");

const log = debug("cutils:logger");
const error = debug("cutils:prod:error");

const http = debug("cutils:http");
const auth = debug("cutils:auth");
const location = debug("cutils:location");

const router = debug("cutils:router");
const utility = debug("cutils:prod:utility");

const component = debug("cutils:component");
const view = debug("cutils:view");

const sw = debug("cutils:prod:sw");
const cache = debug("cutils:prod:cache");

if (process.env.NODE_ENV === "production") {
  localStorage.debug = "cutils:prod:*";
} else {
  localStorage.debug = "cutils:*";
}

export default {
  http,
  auth,
  location,
  log,
  error,
  router,
  utility,
  component,
  view,
  sw,
  cache
};
