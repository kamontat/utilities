import Vue from "vue";
import VueHead from "vue-head";

import Buefy from "buefy";
import "./assets/scss/app.scss";

import App from "./App.vue";

Vue.use(VueHead);

import logger from "./logger";
import router from "./router";
import store from "./store";

import "./registerServiceWorker";
import "./analytic";

import firebase from "./firebase";
import "firebase/auth";
import "firebase/database";

Vue.use(Buefy);

import "./lib/vcalendar";

Vue.config.productionTip = process.env.NODE_ENV === "production";

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    logger.view.extend("vue")("vue create method");

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        logger.auth("already login");
        store.commit("security/login", user);

        logger.router(`redirect to ${this.$route.query.redirect}`);
        if (this.$route.query.redirect)
          this.$router.replace({
            name: this.$route.query.redirect
          });
      }
    });

    const db = firebase.database();

    db.ref("condo/water").on("child_added", function(snapshot) {
      if (snapshot && snapshot.exists()) {
        const water = snapshot.val();
        store.commit("water/add", {
          water,
          location: "condo"
        });
      }
    });
    db.ref("home/water").on("child_added", function(snapshot) {
      if (snapshot && snapshot.exists()) {
        const water = snapshot.val();
        store.commit("water/add", {
          water,
          location: "home"
        });
      }
    });

    db.ref("condo/electricity").on("child_added", function(snapshot) {
      if (snapshot && snapshot.exists()) {
        const electricity = snapshot.val();
        store.commit("electricity/add", {
          electricity,
          location: "condo"
        });
      }
    });
    db.ref("home/electricity").on("child_added", function(snapshot) {
      if (snapshot && snapshot.exists()) {
        const electricity = snapshot.val();
        store.commit("electricity/add", {
          electricity,
          location: "home"
        });
      }
    });
  }
}).$mount("#app");
